//
//  SecondViewController.m
//  AOFrameworkDemo
//
//  Created by Flavien Simon on 16/02/2016.
//  Copyright © 2016 AppOuest. All rights reserved.
//

#import "SecondViewController.h"
#import "UIViewController+AOInternetBanner.h"


@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated
{
    [self startMonitoringInternetConnection];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
