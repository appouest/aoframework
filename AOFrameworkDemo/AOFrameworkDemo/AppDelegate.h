//
//  AppDelegate.h
//  AOFrameworkDemo
//
//  Created by Aymeric De Abreu on 03/12/2015.
//  Copyright © 2015 AppOuest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

