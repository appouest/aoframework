//
//  Testself.m
//  AOFrameworkDemo
//
//  Created by Flavien Simon on 19/01/2016.
//  Copyright © 2016 AppOuest. All rights reserved.
//

#import "TestAlertView.h"

@implementation TestAlertView

- (void)customize
{
    self.titleLabel.textColor = [UIColor blackColor];
    self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    self.messageLabel.textColor = [UIColor blackColor];
    
    self.separatorColor = [UIColor blackColor];
    self.cornerRadius = 20;
    
    self.buttonHeight = 50;
    self.cancelButtonPositionRight = YES;
    self.titleHeight = 50;
    self.titleTopPadding = 20;
    self.titleBottomPadding = 20;
    self.messageBottomPadding = 20;
    
    self.disappearAnimationType = DQAlertViewAnimationTypeFlyBottom;
    
    [self setBackgroundColor:[UIColor whiteColor]];
    
    if (self.cancelButton)
    {
        [self.cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        [self.cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        [self.cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateApplication];
        [self.cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateReserved];
        [self.cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateDisabled];
    }
    
    [self.otherButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.otherButton setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    [self.otherButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [self.otherButton setTitleColor:[UIColor blackColor] forState:UIControlStateApplication];
    [self.otherButton setTitleColor:[UIColor blackColor] forState:UIControlStateReserved];
    [self.otherButton setTitleColor:[UIColor blackColor] forState:UIControlStateDisabled];
    
}

@end
