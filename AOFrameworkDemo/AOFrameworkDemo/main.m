//
//  main.m
//  AOFrameworkDemo
//
//  Created by Aymeric De Abreu on 03/12/2015.
//  Copyright © 2015 AppOuest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
