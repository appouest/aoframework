//
//  ModalViewController.m
//  AOFrameworkDemo
//
//  Created by Flavien Simon on 16/02/2016.
//  Copyright © 2016 AppOuest. All rights reserved.
//

#import "ModalViewController.h"
#import "UIViewController+AOInternetBanner.h"


@interface ModalViewController ()

@end

@implementation ModalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [self startMonitoringInternetConnection];
}

- (IBAction)done:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
