//
//  ViewController.m
//  AOFrameworkDemo
//
//  Created by Aymeric De Abreu on 03/12/2015.
//  Copyright © 2015 AppOuest. All rights reserved.
//

#import "ViewController.h"
#import "TestAlertView.h"

#import "UIViewController+AOInternetBanner.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

}

-(void) viewWillAppear:(BOOL)animated
{
    [self startMonitoringInternetConnection];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
