//
//  AOFramework.h
//  AOFramework
//
//  Created by Aymeric De Abreu on 17/08/2015.
//  Copyright (c) 2015 appouest. All rights reserved.
//

#ifndef AOFramework_h
#define AOFramework_h

//UIKit
#import "UIImage+AOAdditions.h"
#import "UIView+AOAdditions.h"
#import "UIColor+AOAdditions.h"
#import "UIButton+AOAdditions.h"
#import "UIViewController+AOAdditions.h"
#import "UIImageEffects.h"
#import "AORemoteImageProvider.h"
#import "AOCircleImageView.h"
#import "AOCircleView.h"
#import "AOKeepBackgroundAnimation.h"

//Components
#import "SlideOverNavigationController.h"
#import "UIViewController+SlideOver.h"

//Foundation
#import "NSDictionary+AOAdditions.h"
#import "NSString+AOAdditions.h"
#import "NSDate+AOAdditions.h"
#import "NSObject+AOAdditions.h"

//Assets
#import "AssetProxy.h"
#import "AssetsProvider.h"

#endif
