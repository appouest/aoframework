//
//  CircleImageView.m
//  Pods
//
//  Created by Flavien Simon on 16/12/2015.
//
//

#import "AOCircleImageView.h"
#import "UIView+AOAdditions.h"

@implementation AOCircleImageView

    - (void)layoutSubviews
    {
        [super layoutSubviews];
        self.layer.cornerRadius = self.height/2;
    }

@end
