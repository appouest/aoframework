//
//  AOCircleView.m
//  Pods
//
//  Created by Flavien Simon on 03/03/2016.
//
//

#import "AOCircleView.h"
#import "UIView+AOAdditions.h"

@implementation AOCircleView

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.layer.cornerRadius = self.height/2;
}

@end
