//
//  AOAlertView.m
//  Pods
//
//  Created by Flavien Simon on 19/01/2016.
//
//

#import "AOAlertView.h"

#import <DQAlertView/DQAlertView.h>


@implementation AOAlertView

+ (void)showToastWithTitle:(NSString *)title
             okButtonTitle:(NSString *)okButtonTitle
{
    [self showToastWithTitle:title
                              okButtonTitle:okButtonTitle
                             okButtonAction:nil];
}

+ (void)showToastWithTitle:(NSString *)title
             okButtonTitle:(NSString *)okButtonTitle
            okButtonAction:(void (^)(void))okHandler
{
    [self showAlertViewWithTitle:title
                                        message:nil
                                  okButtonTitle:okButtonTitle
                                 okButtonAction:okHandler];
}

+ (void)showAlertViewWithTitle:(NSString *)title
                       message:(NSString *)msg
                 okButtonTitle:(NSString *)okTitle
{
    [self showAlertViewWithTitle:title
                                        message:msg
                                  okButtonTitle:okTitle
                                 okButtonAction:nil];
}

+ (void)showAlertViewWithTitle:(NSString *)title
                       message:(NSString *)msg
             cancelButtonTitle:(NSString *)cancelTitle
                 okButtonTitle:(NSString *)okTitle
{
    [self showAlertViewWithTitle:title
                                        message:msg
                              cancelButtonTitle:cancelTitle
                                  okButtonTitle:okTitle
                                 okButtonAction:nil];
}

+ (void)showAlertViewWithTitle:(NSString *)title
                       message:(NSString *)msg
                 okButtonTitle:(NSString *)okTitle
                okButtonAction:(void (^)(void))okHandler
{
    [self showAlertViewWithTitle:title
                                        message:msg
                              cancelButtonTitle:nil
                                  okButtonTitle:okTitle
                                 okButtonAction:okHandler];
}

+ (void)showAlertViewWithTitle:(NSString *)title
                       message:(NSString *)msg
             cancelButtonTitle:(NSString *)cancelTitle
                 okButtonTitle:(NSString *)okTitle
                okButtonAction:(void (^)(void))okHandler
{
    [self showAlertViewWithTitle:title
                                        message:msg
                              cancelButtonTitle:cancelTitle
                             cancelButtonAction:nil
                                  okButtonTitle:okTitle
                                 okButtonAction:okHandler];
}

+ (void)showAlertViewWithTitle:(NSString *)title
                       message:(NSString *)msg
             cancelButtonTitle:(NSString *)cancelTitle
            cancelButtonAction:(void (^)(void))cancelHandler
                 okButtonTitle:(NSString *)okTitle
                okButtonAction:(void (^)(void))okHandler
{
    AOAlertView *alertView = [[[self class] alloc] initWithTitle:title
                                                        message:msg
                                              cancelButtonTitle:cancelTitle
                                               otherButtonTitle:okTitle];
    
    alertView.cancelButtonAction = cancelHandler;
    alertView.otherButtonAction = okHandler;
    
    [alertView customize];
    [alertView show];
}

- (void)customize
{
    
}



@end
