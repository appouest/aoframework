//
//  AOAlertView.h
//  Pods
//
//  Created by Flavien Simon on 19/01/2016.
//
//

#import <Foundation/Foundation.h>
#import <DQAlertView/DQAlertView.h>

/*
 
 *********************************************************************************************************
 README

 to use AOAlertView you need : 
 
 #1 subclass AOAlertView
 #2 implements customize method to customize alertview
    customizations are based on DQAlertView, see doc --> https://github.com/dinhquan/DQAlertView

 *********************************************************************************************************
 */


@interface AOAlertView : DQAlertView

+ (void)showToastWithTitle:(NSString *)title
             okButtonTitle:(NSString *)okButtonTitle;

+ (void)showToastWithTitle:(NSString *)title
             okButtonTitle:(NSString *)okButtonTitle
            okButtonAction:(void (^)(void))okHandler;

+ (void)showAlertViewWithTitle:(NSString *)title
                       message:(NSString *)msg
                 okButtonTitle:(NSString *)okTitle;

+ (void)showAlertViewWithTitle:(NSString *)title
                       message:(NSString *)msg
                 okButtonTitle:(NSString *)okTitle
                okButtonAction:(void (^)(void))okHandler;

+ (void)showAlertViewWithTitle:(NSString *)title
                       message:(NSString *)msg
             cancelButtonTitle:(NSString *)cancelTitle
                 okButtonTitle:(NSString *)okTitle;

+ (void)showAlertViewWithTitle:(NSString *)title
                       message:(NSString *)msg
             cancelButtonTitle:(NSString *)cancelTitle
                 okButtonTitle:(NSString *)okTitle
                okButtonAction:(void (^)(void))okHandler;

+ (void)showAlertViewWithTitle:(NSString *)title
                       message:(NSString *)msg
             cancelButtonTitle:(NSString *)cancelTitle
            cancelButtonAction:(void (^)(void))cancelHandler
                 okButtonTitle:(NSString *)okTitle
                okButtonAction:(void (^)(void))okHandler;

- (void)customize;

@end
