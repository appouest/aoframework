//
//  AOKeepBackgroundAnimation.m
//  Pods
//
//  Created by Flavien Simon on 18/12/2015.
//
//

#import "AOKeepBackgroundAnimation.h"

@implementation AOKeepBackgroundAnimation

- (id)init
{
    self = [super init];
    if (self) {
        self.duration = 0.3;
    }
    return self;
}

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return self.duration;
}
// This method can only  be a nop if the transition is interactive and not a percentDriven interactive transition.
- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext
{
    UIView *inView = [transitionContext containerView];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    UIViewController *toVC;
    UIViewController *fromVC;
    
    CGRect dstRect;
    
    if (!self.isPresenting)
    {
        fromVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
        toVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
        [toVC.view setFrame:CGRectMake(0, 0, fromVC.view.frame.size.width, fromVC.view.frame.size.height)];
        dstRect = CGRectMake(0, screenRect.size.height, fromVC.view.frame.size.width, fromVC.view.frame.size.height);
    }
    
    else
    {
        toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
        fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
        [inView addSubview:toVC.view];
        [toVC.view setFrame:CGRectMake(0, screenRect.size.height, fromVC.view.frame.size.width, fromVC.view.frame.size.height)];
        dstRect = CGRectMake(0, 0, fromVC.view.frame.size.width, fromVC.view.frame.size.height);
    }
    
    [UIView animateWithDuration:self.duration
                     animations:^
     {
         [toVC.view setFrame:dstRect];
     }
                     completion:^(BOOL finished)
     {
         if (!self.isPresenting)
         {
             [toVC.view removeFromSuperview];
         }
         [transitionContext completeTransition:YES];
     }];
}



@end
