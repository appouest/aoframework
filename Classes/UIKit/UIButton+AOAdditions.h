//
//  UIButton+AOAdditions.h
//  Pods
//
//  Created by Flavien Simon on 14/12/2015.
//
//

#import <UIKit/UIKit.h>

@interface UIButton (AOAdditions)

- (void)centerVerticallyWithPadding:(float)padding;
- (void)centerVertically;

@end
