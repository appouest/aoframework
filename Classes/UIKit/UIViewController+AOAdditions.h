//
//  UIViewController+AOAdditions.h
//  Pods
//
//  Created by Flavien Simon on 14/12/2015.
//
//

#import <UIKit/UIKit.h>

@interface UIViewController (AOAdditions)

- (void)removeFromContainer;
- (void)addInContainer:(UIViewController*)containerViewController;
- (void)addInContainer:(UIViewController*)containerViewController inView:(UIView*)contentView;

@end
