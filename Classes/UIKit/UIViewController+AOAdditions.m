//
//  UIViewController+AOAdditions.m
//  Pods
//
//  Created by Flavien Simon on 14/12/2015.
//
//

#import "UIViewController+AOAdditions.h"

@implementation UIViewController (AOAdditions)

- (void)removeFromContainer
{
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (void)addInContainer:(UIViewController*)containerViewController
{
    [self addInContainer:containerViewController inView:containerViewController.view];
}

- (void)addInContainer:(UIViewController*)containerViewController inView:(UIView*)contentView
{
    [containerViewController addChildViewController:self];
    self.view.frame = contentView.bounds;
    [contentView addSubview:self.view];
    [self didMoveToParentViewController:self];
}

@end
