//
//  UIColor+AOAdditions.h
//  AOFramework
//
//  Created by Aymeric De Abreu on 20/08/2015.
//  Copyright (c) 2015 appouest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (AOAdditions)

+ (void)registerColor:(UIColor *)color forName:(NSString *)name;

+ (instancetype)colorWithString:(NSString *)string;
- (instancetype)initWithString:(NSString *)string;

+ (UIColor*) colorFromRGB:(NSInteger)rgbValue;


@end
