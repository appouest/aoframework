//
//  AORemoteImageProvider.m
//  LPCR
//
//  Created by Aymeric De Abreu on 25/09/2015.
//  Copyright © 2015 appouest. All rights reserved.
//

#import "AORemoteImageProvider.h"
#import "SVProgressHUD.h"

@interface AORemoteImageProvider ()

@property (nonatomic, strong) NSURL* urlToFetch;

@end

@implementation AORemoteImageProvider

- (instancetype)initWithThumbnail:(UIImage*)thumbnail andUrl:(NSString*)url
{
    self = [self initWithPlaceholderItem:thumbnail];
    self.urlToFetch = [NSURL URLWithString:url];
    return self;
}


- (id)item
{
    dispatch_sync(dispatch_get_main_queue(), ^{
        [SVProgressHUD show];
    });
    NSData* data = [NSData dataWithContentsOfURL:self.urlToFetch];
    dispatch_sync(dispatch_get_main_queue(), ^{
        [SVProgressHUD popActivity];
    });
    return [UIImage imageWithData:data];
}

@end
