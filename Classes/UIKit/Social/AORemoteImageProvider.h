//
//  AORemoteImageProvider.h
//  LPCR
//
//  Created by Aymeric De Abreu on 25/09/2015.
//  Copyright © 2015 appouest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AORemoteImageProvider : UIActivityItemProvider

- (instancetype)initWithThumbnail:(UIImage*)thumbnail andUrl:(NSString*)url;

@end
