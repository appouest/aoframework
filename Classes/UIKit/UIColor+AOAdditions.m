//
//  UIColor+AOAdditions.m
//  AOFramework
//
//  Created by Aymeric De Abreu on 20/08/2015.
//  Copyright (c) 2015 appouest. All rights reserved.
//

#import "UIColor+AOAdditions.h"

@implementation UIColor (AOAdditions)

+ (NSMutableDictionary *)colors
{
    static NSMutableDictionary *sharedColors = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        sharedColors = [@{@"black": [self blackColor], // 0.0 white
                    @"darkgray": [self darkGrayColor], // 0.333 white
                    @"lightgray": [self lightGrayColor], // 0.667 white
                    @"white": [self whiteColor], // 1.0 white
                    @"gray": [self grayColor], // 0.5 white
                    @"red": [self redColor], // 1.0, 0.0, 0.0 RGB
                    @"green": [self greenColor], // 0.0, 1.0, 0.0 RGB
                    @"blue": [self blueColor], // 0.0, 0.0, 1.0 RGB
                    @"cyan": [self cyanColor], // 0.0, 1.0, 1.0 RGB
                    @"yellow": [self yellowColor], // 1.0, 1.0, 0.0 RGB
                    @"magenta": [self magentaColor], // 1.0, 0.0, 1.0 RGB
                    @"orange": [self orangeColor], // 1.0, 0.5, 0.0 RGB
                    @"purple": [self purpleColor], // 0.5, 0.0, 0.5 RGB
                    @"brown": [self brownColor], // 0.6, 0.4, 0.2 RGB
                    @"clear": [self clearColor]} mutableCopy];
    });
    
    return sharedColors;
}

+ (void)registerColor:(UIColor *)color forName:(NSString *)name
{
    name = [name lowercaseString];
    
#ifdef DEBUG
    NSAssert([self colors][name] == nil, @"Cannot re-register the color '%@' as this is already assigned", name);
#endif
        
    [self colors][[name lowercaseString]] = color;
}

+ (instancetype)colorWithString:(NSString *)string
{
    //convert to lowercase
    string = [string lowercaseString];
    
    //try standard colors first
    UIColor *color = nil;
    
    color = [self colors][string];
    
    if (color)
    {
        return color;
    }
    
    //create new instance
    return [[self alloc] initWithString:string useLookup:NO];
}

- (instancetype)initWithString:(NSString *)string
{
    return [self initWithString:string useLookup:YES];
}

- (instancetype)initWithString:(NSString *)string useLookup:(BOOL)lookup
{
    //convert to lowercase
    string = [string lowercaseString];
    
    if (lookup)
    {
        //try standard colors
        UIColor *color = nil;
        color = [[self class] colors][string];
        if (color)
        {
            return ((self = color));
        }
    }
    
    //try hex
    string = [string stringByReplacingOccurrencesOfString:@"#" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"0x" withString:@""];
    switch ([string length])
    {
        case 0:
        {
            string = @"00000000";
            break;
        }
        case 3:
        {
            NSString *red = [string substringWithRange:NSMakeRange(0, 1)];
            NSString *green = [string substringWithRange:NSMakeRange(1, 1)];
            NSString *blue = [string substringWithRange:NSMakeRange(2, 1)];
            string = [NSString stringWithFormat:@"%1$@%1$@%2$@%2$@%3$@%3$@ff", red, green, blue];
            break;
        }
        case 6:
        {
            string = [string stringByAppendingString:@"ff"];
            break;
        }
        case 8:
        {
            //do nothing
            break;
        }
        default:
        {
            
#ifdef DEBUG
            //unsupported format
            NSLog(@"Unsupported color string format: %@", string);
#endif
            return nil;
        }
    }
    uint32_t rgba;
    NSScanner *scanner = [NSScanner scannerWithString:string];
    [scanner scanHexInt:&rgba];
    return [self initWithRGBAValue:rgba];
}

- (instancetype)initWithRGBAValue:(uint32_t)rgba
{
    CGFloat red = ((rgba & 0xFF000000) >> 24) / 255.0f;
    CGFloat green = ((rgba & 0x00FF0000) >> 16) / 255.0f;
    CGFloat blue = ((rgba & 0x0000FF00) >> 8) / 255.0f;
    CGFloat alpha = (rgba & 0x000000FF) / 255.0f;
    return [self initWithRed:red green:green blue:blue alpha:alpha];
}

+ (UIColor *)colorFromRGB:(NSInteger)rgbValue
{
    return [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0];
}

@end
