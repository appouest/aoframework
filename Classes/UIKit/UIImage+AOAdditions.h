//
//  UIImage+AOAdditions.h
//  AOFramework
//
//  Created by Aymeric De Abreu on 17/08/2015.
//  Copyright (c) 2015 appouest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (AOAdditions)

+ (UIImage *)imageNamed:(NSString *)name
              withColor:(UIColor *)color;
+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)imageFromColor:(UIColor *)color;
+ (UIImage *)image:(UIImage *)image
         withColor:(UIColor *)color;
+ (UIImage*) mergeTwoImagesWithTop:(UIImage*)topImage
                         andBottom:(UIImage*) bottomImage;

- (UIImage *)fixOrientation;

@end
