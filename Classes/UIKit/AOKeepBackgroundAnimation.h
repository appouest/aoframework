//
//  AOKeepBackgroundAnimation.h
//  Pods
//
//  Created by Flavien Simon on 18/12/2015.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AOKeepBackgroundAnimation : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) BOOL isPresenting;

//default is 0.3
@property (nonatomic,assign) CGFloat duration;

@end