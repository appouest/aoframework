//
//  NSObject+AOAdditions.m
//  Pods
//
//  Created by Flavien Simon on 14/12/2015.
//
//

#import "NSObject+AOAdditions.h"

@implementation NSObject (AOAdditions)

+(NSString*) className
{
    return NSStringFromClass([self class]);
}

@end
