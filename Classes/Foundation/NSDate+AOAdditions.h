//
//  NSDate+Comparison.h
//  Fizzer
//
//  Created by Jean-Baptiste Denoual on 10/12/2015.
//  Copyright © 2015 AppOuest. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (AOAdditions)

/**
 *   This method allow you to quickly create a NSDate at a given moment.
 *   @param year The year of the NSDate to create
 *   @param month The month of the NSDate to create
 *   @param day The day of the NSDate to create
 *   @param hour The hour of the NSDate to create
 *   @param minute The minute of the NSDate to create
 *   @param second The second of the NSDate to create
 *   @returns The newly created date ...
 */
+ (NSDate *)dateWithYear:(int)year month:(int)month day:(int)day hour:(int)hour minute:(int)minute second:(int)second;

/**
 *   @returns The receiver date at 0h00 morning.
 */
- (NSDate *)beginningOfDay;

/**
 *   @returns The first day at 0h00 of the month of the receiver.
 */
- (NSDate *)beginningOfMonth;

/**
 *   @returns The 01/01 at 0h00 morning of the year of the receiver.
 */
- (NSDate *)beginningOfYear;

/**
 *   @returns The receiver date at 23h59.
 */
- (NSDate *)endOfDay;

/**
 *   @returns The last day at 23h59 of the month of the receiver.
 */
- (NSDate *)endOfMonth;

/**
 *   @returns The 31/12 at 23h59  of the year of the receiver.
 */
- (NSDate *)endOfYear;

/**
 *   @returns The monday at 00:00:00 just before the receiver's date.
 *   @note If receiver's date is a Monday @ 00:00:00 ... It will return the same dateL.
 */
- (NSDate *)mondayOfWeek;

/**
 *   @returns The saturday at 23:59:59 in the week of the receiver.
 */
- (NSDate *)saturdayOfWeek;

/**
 *   @returns The sunday at 23:59:59 in the week of the receiver.
 */
- (NSDate *)sundayOfWeek;

/**
 *   @returns The number of days in the month of the receiver.
 */
- (int)daysInMonth;

/**
 *   Methode used to create a new NSDate by adding things to the current date ... So: you can travel in future ...
 *   @param years the number of years to add
 *   @param months  the number of months to add
 *   @param weeks  the number of weeks to add
 *   @param days the number of days to add
 *   @param hours the number of hours to add
 *   @param minutes the number of minutes to add
 *   @param seconds the number of seconds to add
 *   @returns The newly created NSDate
 */
- (NSDate *)dateByAddingYears:(int)years months:(int)months weeks:(int)weeks days:(int)days hours:(int)hours minutes:(int)minutes seconds:(int)seconds;

/**
 *   Methode used to create a new NSDate by removing things to the current date ... So: you can travel in past ...
 *   @param years the number of years to remove
 *   @param months  the number of months to remove
 *   @param weeks  the number of weeks to remove
 *   @param days the number of days to remove
 *   @param hours the number of hours to remove
 *   @param minutes the number of minutes to remove
 *   @param seconds the number of seconds to remove
 *   @returns The newly created NSDate
 */
- (NSDate *)dateBySubstractingYears:(int)years months:(int)months weeks:(int)weeks days:(int)days hours:(int)hours minutes:(int)minutes seconds:(int)seconds;

/**
 *   Create a new NSDate by adding a certain number of years
 *   @param years The count of years to add
 *   @returns a new NSDate in the future
 */
- (NSDate *)dateByAddingYears:(int)years;

/**
 *   Create a new NSDate by substracting a certain number of years;
 *   @param years; The count of seconds to subsctract
 *   @returns a new NSDate in the past
 */
- (NSDate *)dateBySubstractingYears:(int)years;

/**
 *   Create a new NSDate by adding a certain number of months
 *   @param months The count of months to add
 *   @returns a new NSDate in the future
 */
- (NSDate *)dateByAddingMonths:(int)months;

/**
 *   Create a new NSDate by substracting a certain number of months
 *   @param months The count of months to subsctract
 *   @returns a new NSDate in the past
 */
- (NSDate *)dateBySubstractingMonths:(int)months;

/**
 *   Create a new NSDate by adding a certain number of weeks
 *   @param months The count of weeks to add
 *   @returns a new NSDate in the future
 */
- (NSDate *)dateByAddingWeeks:(int)weeks;

/**
 *   Create a new NSDate by substracting a certain number of weeks
 *   @param weeks The count of weeks to subsctract
 *   @returns a new NSDate in the past
 */
- (NSDate *)dateBySubstractingWeeks:(int)weeks;

/**
 *   Create a new NSDate by adding a certain number of days
 *   @param days The count of days to add
 *   @returns a new NSDate in the future
 */
- (NSDate *)dateByAddingDays:(int)days;

/**
 *   Create a new NSDate by substracting a certain number of days
 *   @param days The count of days to subsctract
 *   @returns a new NSDate in the past
 */
- (NSDate *)dateBySubstractingDays:(int)days;

/**
 *   Create a new NSDate by adding a certain number of hours
 *   @param hours The count of hours to add
 *   @returns a new NSDate in the future
 */
- (NSDate *)dateByAddingHours:(int)hours;

/**
 *   Create a new NSDate by substracting a certain number of hours
 *   @param hours The count of hours to subsctract
 *   @returns a new NSDate in the past
 */
- (NSDate *)dateBySubstractingHours:(int)hours;

/**
 *   Create a new NSDate by adding a certain number of minutes
 *   @param minutes The count of minutes to add
 *   @returns a new NSDate in the future
 */
- (NSDate *)dateByAddingMinutes:(int)minutes;

/**
 *   Create a new NSDate by substracting a certain number of minutes
 *   @param minutes The count of minutes to subsctract
 *   @returns a new NSDate in the past
 */
- (NSDate *)dateBySubstractingMinutes:(int)minutes;

/**
 *   Create a new NSDate by adding a certain number of seconds
 *   @param seconds The count of seconds to add
 *   @returns a new NSDate in the future
 */
- (NSDate *)dateByAddingSeconds:(int)seconds;

/**
 *   Create a new NSDate by substracting a certain number of seconds
 *   @param seconds The count of seconds to subsctract
 *   @returns a new NSDate in the past
 */
- (NSDate *)dateBySubstractingSeconds:(int)seconds;

/**
 *   @returns Gives a newly created NSDate with the date of the receiver by adding 1 year
 */
- (NSDate *)nextYear;

/**
 *   @returns Gives a newly created NSDate with the date of the receiver by substracting 1 year
 */
- (NSDate *)previousYear;

/**
 *   @returns Gives a newly created NSDate with the date of the receiver by adding 1 month
 */
- (NSDate *)nextMonth;

/**
 *   @returns Gives a newly created NSDate with the date of the receiver by substracting 1 month
 */
- (NSDate *)previousMonth;

/**
 *   @returns Gives a newly created NSDate with the date of the receiver by adding 1 week
 */
- (NSDate *)nextWeek;

/**
 *   @returns Gives a newly created NSDate with the date of the receiver by substracting 1 week
 */
- (NSDate *)previousWeek;

/**
 *   Method used to determines is a NSDate is in the same day than the receiver
 *   @param anotherDate the date to test
 *   @returns YES if two dates are in the same day
 */
- (BOOL)isSameDay:(NSDate *)anotherDate;

/**
 *   Method used to determines the receiver is in the same day than current date
 *   @returns YES if receiver is today
 */
- (BOOL)isToday;

/**
 *   Method used to determines the receiver is in the same day than current next day
 *   @returns YES if receiver is Tomorow
 */
- (BOOL)isTomorow;

/**
 *   Method used to determines the receiver is in the same day than current previous day
 *   @returns YES if receiver is yesterday
 */
- (BOOL)isYesterday;

/**
 *   @returns Gives a NSDate for tomorow at 0.00 AM
 */
+ (NSDate *)tomorow;

/**
 *   @returns Gives a NSDate for yesterday at 0.00 AM
 */
+ (NSDate *)yesterday;

/**
 *        Gives a formated description of the date for HTTP ...  rfc1123 format with TZ forced to GMT
 *    @see rfc2616 [3.3.1].  For example: "Mon, 01 Jan 2001 00:00:00 GMT"
 *        @returns A NSString representing the formated date
 */
- (NSString *)descriptionWithHTTPFormat;

- (NSString *)descriptionWithSimpleFormat;

- (NSInteger)daysComponentToDate:(NSDate *)date;
- (NSInteger)hoursComponentToDate:(NSDate *)date;
- (NSInteger)minutesComponentToDate:(NSDate *)date;
- (NSInteger)secondesComponentToDate:(NSDate *)date;

- (BOOL)isWeekEnd;

/**
 * Round a date with minutes interval
 * ex : date is 12:13 calling [date dateByRoundingWithMinutesInterval:20] --> 12:20
 * ex : date is 12:13 calling [date dateByRoundingWithMinutesInterval:30] --> 12:30
 */
- (NSDate*) dateByRoundingWithMinutesInterval:(NSInteger)minutesInterval;


@end
