//
//  NSDictionary+NSNull.m
//  AOFramework
//
//  Created by Aymeric De Abreu on 18/08/2015.
//  Copyright (c) 2015 appouest. All rights reserved.
//

#import "NSDictionary+AOAdditions.h"

@implementation NSDictionary (AOAdditions)

- (NSDictionary*)dictionaryByStrippingNSNullValue
{
    NSMutableDictionary* result = [self mutableCopy];
    NSArray* keys = [result allKeys];
    for (id key in keys) {
        if ([result[key] isEqual:[NSNull null]]) {
            [result removeObjectForKey:key];
        }
        if ([result[key] isKindOfClass:[NSDictionary class]]) {
            NSDictionary* dic = result[key];
            result[key] = [dic dictionaryByStrippingNSNullValue];
        }
        if ([result[key] isKindOfClass:[NSArray class]]) {
            NSMutableArray* array = [result[key] mutableCopy];
            for (int i = 0; i < array.count; i++) {
                if ([array[i] isKindOfClass:[NSDictionary class]]) {
                    array[i] = [array[i] dictionaryByStrippingNSNullValue];
                }
            }
            result[key] = array;
        }
    }
    return result;
}

@end
