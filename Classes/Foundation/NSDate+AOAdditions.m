//
//  NSDate+Comparison.m
//  Fizzer
//
//  Created by Jean-Baptiste Denoual on 10/12/2015.
//  Copyright © 2015 AppOuest. All rights reserved.
//

#import "NSDate+AOAdditions.h"

@implementation NSDate (AOAdditions)

+ (NSDate *)dateWithYear:(int)year month:(int)month day:(int)day hour:(int)hour minute:(int)minute second:(int)second
{
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    
    [comps setYear:year];
    [comps setMonth:month];
    [comps setDay:day];
    [comps setHour:hour];
    [comps setMinute:minute];
    [comps setSecond:second];
    
    return [[NSCalendar currentCalendar] dateFromComponents:comps];
}

#pragma mark Begin of and End Of
- (NSDate *)beginningOfDay
{
    NSCalendar       *currentCalendar    = [NSCalendar currentCalendar];
    int               calendarComponents = (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour);
    NSDateComponents *comps              = [currentCalendar components:calendarComponents fromDate:self];
    
    [comps setHour:0];
    [comps setMinute:0];
    [comps setSecond:0];
    
    return [currentCalendar dateFromComponents:comps];
}

- (NSDate *)beginningOfMonth
{
    NSCalendar       *currentCalendar    = [NSCalendar currentCalendar];
    int               calendarComponents = (NSCalendarUnitYear | NSCalendarUnitMonth);
    NSDateComponents *comps              = [currentCalendar components:calendarComponents fromDate:self];
    
    [comps setDay:1];
    [comps setHour:0];
    [comps setMinute:00];
    [comps setSecond:00];
    
    return [currentCalendar dateFromComponents:comps];
}

- (NSDate *)beginningOfYear
{
    NSCalendar       *currentCalendar    = [NSCalendar currentCalendar];
    int               calendarComponents = (NSCalendarUnitYear);
    NSDateComponents *comps              = [currentCalendar components:calendarComponents fromDate:self];
    
    [comps setMonth:1];
    [comps setDay:1];
    [comps setHour:0];
    [comps setMinute:0];
    [comps setSecond:0];
    
    return [currentCalendar dateFromComponents:comps];
}

- (NSDate *)endOfDay
{
    NSCalendar       *currentCalendar    = [NSCalendar currentCalendar];
    int               calendarComponents = (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour);
    NSDateComponents *comps              = [currentCalendar components:calendarComponents fromDate:self];
    
    [comps setHour:23];
    [comps setMinute:59];
    [comps setSecond:59];
    
    return [currentCalendar dateFromComponents:comps];
}

- (NSDate *)endOfMonth
{
    NSCalendar       *currentCalendar    = [NSCalendar currentCalendar];
    int               calendarComponents = (NSCalendarUnitYear | NSCalendarUnitMonth);
    NSDateComponents *comps              = [currentCalendar components:calendarComponents fromDate:self];
    
    [comps setDay:[self daysInMonth]];
    [comps setHour:23];
    [comps setMinute:59];
    [comps setSecond:59];
    
    return [currentCalendar dateFromComponents:comps];
}

- (NSDate *)endOfYear
{
    NSCalendar       *currentCalendar    = [NSCalendar currentCalendar];
    int               calendarComponents = (NSCalendarUnitYear);
    NSDateComponents *comps              = [currentCalendar components:calendarComponents fromDate:self];
    
    [comps setMonth:12];
    [comps setDay:31];
    [comps setHour:23];
    [comps setMinute:59];
    [comps setSecond:59];
    
    return [currentCalendar dateFromComponents:comps];
}

- (NSDate *)mondayOfWeek
{
    NSCalendar       *currentCalendar    = [NSCalendar currentCalendar];
    int               calendarComponents = (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekdayOrdinal | NSCalendarUnitWeekOfYear| NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekday);
    NSDateComponents *comps              = [currentCalendar components:calendarComponents fromDate:self];
    
    [comps setWeekday:2];
    [comps setHour:0];
    [comps setMinute:00];
    [comps setSecond:00];
    
    NSDate *ret = [currentCalendar dateFromComponents:comps];
    
    if ([ret timeIntervalSinceDate:self] > 0) {
        ret = [ret dateBySubstractingDays:7];
    }
    
    return ret;
}

- (NSDate *)saturdayOfWeek
{
    return [[[self mondayOfWeek] dateByAddingDays:5] endOfDay];
}

- (NSDate *)sundayOfWeek
{
    return [[[self mondayOfWeek] dateByAddingDays:6] endOfDay];
}

#pragma mark Date by adding / substracting
- (NSDate *)dateByAddingYears:(int)years
                       months:(int)months
                        weeks:(int)weeks
                         days:(int)days
                        hours:(int)hours
                      minutes:(int)minutes
                      seconds:(int)seconds
{
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    
    [comps setYear:years];
    [comps setMonth:months];
    [comps setWeekOfYear:weeks];
    [comps setDay:days];
    [comps setHour:hours];
    [comps setMinute:minutes];
    [comps setSecond:seconds];
    
    return [[NSCalendar currentCalendar] dateByAddingComponents:comps toDate:self options:0];
}

- (NSDate *)dateBySubstractingYears:(int)years
                             months:(int)months
                              weeks:(int)weeks
                               days:(int)days
                              hours:(int)hours
                            minutes:(int)minutes
                            seconds:(int)seconds
{
    return [self dateByAddingYears:-years
                            months:-months
                             weeks:-weeks
                              days:-days
                             hours:-hours
                           minutes:-minutes
                           seconds:-seconds];
}

- (NSDate *)dateByAddingYears:(int)years
{
    return [self dateByAddingYears:years months:0 weeks:0 days:0 hours:0 minutes:0 seconds:0];
}

- (NSDate *)dateBySubstractingYears:(int)years
{
    return [self dateBySubstractingYears:years months:0 weeks:0 days:0 hours:0 minutes:0 seconds:0];
}

- (NSDate *)dateByAddingMonths:(int)months
{
    return [self dateByAddingYears:0 months:months weeks:0 days:0 hours:0 minutes:0 seconds:0];
}

- (NSDate *)dateBySubstractingMonths:(int)months
{
    return [self dateBySubstractingYears:0 months:months weeks:0 days:0 hours:0 minutes:0 seconds:0];
}

- (NSDate *)dateByAddingWeeks:(int)weeks
{
    return [self dateByAddingYears:0 months:0 weeks:weeks days:0 hours:0 minutes:0 seconds:0];
}

- (NSDate *)dateBySubstractingWeeks:(int)weeks
{
    return [self dateBySubstractingYears:0 months:0 weeks:weeks days:0 hours:0 minutes:0 seconds:0];
}

- (NSDate *)dateByAddingDays:(int)days
{
    return [self dateByAddingYears:0 months:0 weeks:0 days:days hours:0 minutes:0 seconds:0];
}

- (NSDate *)dateBySubstractingDays:(int)days
{
    return [self dateBySubstractingYears:0 months:0 weeks:0 days:days hours:0 minutes:0 seconds:0];
}

- (NSDate *)dateByAddingHours:(int)hours
{
    return [self dateByAddingYears:0 months:0 weeks:0 days:0 hours:hours minutes:0 seconds:0];
}

- (NSDate *)dateBySubstractingHours:(int)hours
{
    return [self dateBySubstractingYears:0 months:0 weeks:0 days:0 hours:hours minutes:0 seconds:0];
}

- (NSDate *)dateByAddingMinutes:(int)minutes
{
    return [self dateByAddingYears:0 months:0 weeks:0 days:0 hours:0 minutes:minutes seconds:0];
}

- (NSDate *)dateBySubstractingMinutes:(int)minutes
{
    return [self dateBySubstractingYears:0 months:0 weeks:0 days:0 hours:0 minutes:minutes seconds:0];
}

- (NSDate *)dateByAddingSeconds:(int)seconds
{
    return [self dateByAddingYears:0 months:0 weeks:0 days:0 hours:0 minutes:0 seconds:seconds];
}

- (NSDate *)dateBySubstractingSeconds:(int)seconds
{
    return [self dateBySubstractingYears:0 months:0 weeks:0 days:0 hours:0 minutes:0 seconds:seconds];
}

#pragma mark Previous / Next
- (NSDate *)nextYear
{
    return [self dateByAddingYears:1];
}

- (NSDate *)previousYear
{
    return [self dateBySubstractingYears:1];
}

- (NSDate *)nextMonth
{
    return [self dateByAddingMonths:1];
}

- (NSDate *)previousMonth
{
    return [self dateByAddingMonths:-1];
}

- (NSDate *)nextWeek
{
    return [self dateByAddingWeeks:1];
}

- (NSDate *)previousWeek
{
    return [self dateBySubstractingWeeks:1];
}

#pragma mark Others
- (int)daysInMonth
{
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    NSRange     days            = [currentCalendar rangeOfUnit:NSCalendarUnitDay
                                                        inUnit:NSCalendarUnitMonth
                                                       forDate:self];
    
    return (int)days.length;
}

#pragma mark NSDate comparisons
- (BOOL)isSameDay:(NSDate *)anotherDate
{
    NSCalendar       *calendar    = [NSCalendar currentCalendar];
    NSDateComponents *components1 = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:self];
    NSDateComponents *components2 =
    [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:anotherDate];
    
    return [components1 year] == [components2 year] && [components1 month] == [components2 month] && [components1 day] == [components2 day];
}

- (BOOL)isToday
{
    return [self isSameDay:[NSDate date]];
}

- (BOOL)isTomorow
{
    return [self isSameDay:[NSDate tomorow]];
}

- (BOOL)isYesterday
{
    return [self isSameDay:[NSDate yesterday]];
}

+ (NSDate *)tomorow
{
    return [[[NSDate date] dateByAddingDays:1] beginningOfDay];
}

+ (NSDate *)yesterday
{
    return [[[NSDate date] dateBySubstractingDays:1] beginningOfDay];
}

- (NSString *)descriptionWithHTTPFormat
{
    static NSDateFormatter *dateFormatter = nil;
    
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *us_en_locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [dateFormatter setLocale:us_en_locale];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        [dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss 'GMT'"];
    }
    
    return [dateFormatter stringFromDate:self];
}

- (NSString *)descriptionWithSimpleFormat
{
    static NSDateFormatter *dateFormatterSimple = nil;
    
    if (dateFormatterSimple == nil) {
        dateFormatterSimple = [[NSDateFormatter alloc] init];
        NSLocale *us_en_locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [dateFormatterSimple setLocale:us_en_locale];
        [dateFormatterSimple setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        [dateFormatterSimple setDateFormat:@"dd/MMM/yyyy"];
    }
    
    return [dateFormatterSimple stringFromDate:self];
}

- (NSInteger)daysComponentToDate:(NSDate *)date
{
    if (!date) {
        return 0;
    }
    
    NSDateComponents *difference = [self differenceComponentsWithDate:date];
    
    return [difference day];
}

- (NSInteger)hoursComponentToDate:(NSDate *)date
{
    if (!date) {
        return 0;
    }
    
    NSDateComponents *difference = [self differenceComponentsWithDate:date];
    
    return [difference hour];
}

- (NSInteger)minutesComponentToDate:(NSDate *)date
{
    if (!date) {
        return 0;
    }
    
    NSDateComponents *difference = [self differenceComponentsWithDate:date];
    
    return [difference minute];
}

- (NSInteger)secondesComponentToDate:(NSDate *)date
{
    if (!date) {
        return 0;
    }
    
    NSDateComponents *difference = [self differenceComponentsWithDate:date];
    
    return [difference second];
}

- (NSDateComponents *)differenceComponentsWithDate:(NSDate *)date
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond startDate:&fromDate
                 interval:NULL forDate:self];
    [calendar rangeOfUnit:NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond startDate:&toDate
                 interval:NULL forDate:date];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond
                                               fromDate:fromDate toDate:toDate options:0];
    
    return difference;
}

- (BOOL)isWeekEnd
{
    if([[self saturdayOfWeek] compare:self]==NSOrderedSame ||
       [[self sundayOfWeek] compare:self]==NSOrderedSame) {
        
        return YES;
    }
    
    return NO;
}

- (NSDate*) dateByRoundingWithMinutesInterval:(NSInteger)minutesInterval
{
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [currentCalendar components:(NSCalendarUnitMinute) fromDate:self];
    
    NSInteger minutes = components.minute;
    NSInteger diff = 0;
    
    for (NSInteger i=0; i <= 60; i += minutesInterval)
    {
        if (minutes < i)
        {
            diff = i - minutes;
            break;
        }
    }
    
    NSDateComponents *resetSecondsComponents = [currentCalendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:self];
    NSDate *dateWithoutSeconds = [currentCalendar dateFromComponents:resetSecondsComponents];
    
    NSDate *newDate = [dateWithoutSeconds dateByAddingTimeInterval:diff*60];
    return newDate;
}

@end
