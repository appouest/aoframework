//
//  NSObject+AOAdditions.h
//  Pods
//
//  Created by Flavien Simon on 14/12/2015.
//
//

#import <Foundation/Foundation.h>

@interface NSObject (AOAdditions)

+(NSString*) className;

@end
