//
//  NSString+AOAdditions.h
//  LPCR
//
//  Created by Aymeric De Abreu on 21/08/2015.
//  Copyright (c) 2015 appouest. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (AOAdditions)

- (NSString*)stringByEscapingForHTML;
- (NSString *)stringByUnescapingFromHTML;
- (NSString *)stringByEncodingURL;
- (BOOL)isValidEmail;
-(NSString*) capitalizedSentenceString;

@end
