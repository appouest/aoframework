//
//  NSDictionary+NSNull.h
//  AOFramework
//
//  Created by Aymeric De Abreu on 18/08/2015.
//  Copyright (c) 2015 appouest. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (AOAdditions)

- (NSDictionary*)dictionaryByStrippingNSNullValue;

@end
