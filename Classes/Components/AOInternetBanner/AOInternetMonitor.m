//
//  AOInternetMonitor.m
//  Pods
//
//  Created by Flavien Simon on 16/02/2016.
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "AOInternetMonitor.h"
#import "UIViewController+AOInternetBanner.h"
#import "SCNetworkReachability+Multithreading.h"

@interface AOInternetMonitor ()

@property (nonatomic,strong) SCNetworkReachability *reachability;
@property (nonatomic,strong) UIViewController *currentController;

@end

@implementation AOInternetMonitor

#pragma mark - Singleton Methods

+(AOInternetMonitor*)sharedInstance
{
    static AOInternetMonitor *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Setup & Teardown

-(id)init
{
    self = [super init];
    if (self)
    {
        self.reachability = [[SCNetworkReachability alloc] initWithHost:@"http://www.google.fr"];
        [self.reachability observeReachabilityOnDispatchQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)
                                                     callback:^(SCNetworkStatus status) {
                                                         
                                                         [self notifyForStatus:status];
                                                         
                                                     }];
    }
    return self;
}

+ (void) monitorViewController:(UIViewController*)newController
{
    BOOL isNewControllerInAnotherNavigationController = [[AOInternetMonitor sharedInstance] isNewControllerInAnotherNavigationController:newController];
    [AOInternetMonitor sharedInstance].currentController = newController;
    if (isNewControllerInAnotherNavigationController)
    {
        [[AOInternetMonitor sharedInstance] updateStatus];
    }
}

-(void) updateStatus
{
    [self.reachability reachabilityStatusOnDispatchQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)
                                                callback:^(SCNetworkStatus status) {
                                                    
                                                    [self notifyForStatus:status];
                                                    
                                                }];
}

-(void) notifyForStatus:(SCNetworkStatus)status
{
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (status) {
            case SCNetworkStatusNotReachable:
            {
                [self.currentController showInternetError];
                break;
            }
            case SCNetworkStatusReachableViaWiFi:
            {
                [self.currentController hideInternetError];
                break;
            }
            case SCNetworkStatusReachableViaCellular:
            {
                [self.currentController hideInternetError];
                break;
            }
        }
    });
}

-(BOOL) isNewControllerInAnotherNavigationController:(UIViewController*)newController
{
    return self.currentController.navigationController != newController.navigationController;
}

@end