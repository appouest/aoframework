//
//  UIViewController+AOInternetBanner.m
//
//  Created by Flavien Simon on 16/02/2016.
//  Copyright © 2015 AppOuest. All rights reserved.
//

#import "UIViewController+AOInternetBanner.h"
#import "AOInternetMonitor.h"
#import "AOAFMInfoBanner.h"

@implementation UIViewController (AOInternetBanner)


- (void)startMonitoringInternetConnection
{
    [AOInternetMonitor monitorViewController:self];
}

- (void) showInternetError
{
    [AOAFMInfoBanner showWithText:[self localizedStringForKey:@"AOInternetBanner_no_internet" withDefault:@"Pas de connexion Internet"] style:AOAFMInfoBannerStyleError animated:YES];
}


- (void) hideInternetError
{
    [AOAFMInfoBanner hideAll];
}


- (NSString *)localizedStringForKey:(NSString *)key withDefault:(NSString *)defaultString
{
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"AOFramework" ofType:@"bundle"];
    NSBundle *bundle = [NSBundle bundleWithPath:bundlePath];
    NSString *language = [[NSLocale preferredLanguages] count]? [NSLocale preferredLanguages][0]: @"en";
    if (![[bundle localizations] containsObject:language])
    {
        language = [language componentsSeparatedByString:@"-"][0];
    }
    if ([[bundle localizations] containsObject:language])
    {
        bundlePath = [bundle pathForResource:language ofType:@"lproj"];
    }
    bundle = [NSBundle bundleWithPath:bundlePath] ?: [NSBundle mainBundle];
    defaultString = [bundle localizedStringForKey:key value:defaultString table:nil];
    return [[NSBundle mainBundle] localizedStringForKey:key value:defaultString table:nil];
}

@end
