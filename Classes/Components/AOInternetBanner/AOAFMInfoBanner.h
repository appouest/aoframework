//
//  AOAFMInfoBanner.h
//  Pods
//
//  Created by Flavien Simon on 16/02/2016.
//
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, AOAFMInfoBannerStyle) {
    AOAFMInfoBannerStyleError = 0,
    AOAFMInfoBannerStyleInfo,
};

@interface AOAFMInfoBanner : UIView

@property (nonatomic) AOAFMInfoBannerStyle style;
@property (nonatomic) NSString *text;

@property (nonatomic, copy) void (^showCompletionBlock)();
@property (nonatomic, copy) void (^hideCompletionBlock)();
@property (nonatomic, copy) void (^tappedBlock)();

@property (nonatomic) UIFont *font UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIColor *errorBackgroundColor UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIColor *infoBackgroundColor UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIColor *errorTextColor UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIColor *infoTextColor UI_APPEARANCE_SELECTOR;
@property (nonatomic) CGFloat topSpacing UI_APPEARANCE_SELECTOR;
@property (nonatomic) UIColor *customBackgroundColor;
@property (nonatomic) UIColor *customTextColor;

- (id)initWithTargetView:(UIView *)targetView
         viewAboveBanner:(UIView *)viewAboveBanner
    additionalTopSpacing:(CGFloat)additionalTopSpacing;

- (void)show:(BOOL)animated;
- (void)hide:(BOOL)animated;
- (void)show:(BOOL)animated withCompletion:(void (^)())completionBlock;
- (void)hide:(BOOL)animated withCompletion:(void (^)())completionBlock;
- (void)showAndHideAfter:(NSTimeInterval)timeout animated:(BOOL)animated;

+ (instancetype)showWithText:(NSString *)text
                       style:(AOAFMInfoBannerStyle)style
                andHideAfter:(NSTimeInterval)timeout;
+ (instancetype)showWithText:(NSString *)text
                       style:(AOAFMInfoBannerStyle)style
                    animated:(BOOL)animated;
+ (instancetype)showAndHideWithText:(NSString *)text
                              style:(AOAFMInfoBannerStyle)style;
+ (void)hideAll;

@end
