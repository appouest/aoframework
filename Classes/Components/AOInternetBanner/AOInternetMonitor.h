//
//  AOInternetMonitor.h
//  Pods
//
//  Created by Flavien Simon on 16/02/2016.
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AOInternetMonitor : NSObject

+(AOInternetMonitor*)sharedInstance;
+ (void) monitorViewController:(UIViewController*)newController;

@end