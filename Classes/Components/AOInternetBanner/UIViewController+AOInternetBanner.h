//
//  UIViewController+AOInternetBanner.h
//
//  Created by Flavien Simon on 16/02/2016.
//  Copyright © 2015 AppOuest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (AOInternetBanner)

- (void)startMonitoringInternetConnection;

- (void) showInternetError;
- (void) hideInternetError;


@end
