//
//  SlideOverNavigationController.h
//  RTE
//
//  Created by Aymeric De Abreu on 08/10/2015.
//  Copyright © 2015 altays. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+AOAdditions.h"

@interface SlideOverNavigationController : UINavigationController

@property (nonatomic, strong) UIViewController* leftMenuController;

@property (nonatomic, strong) UIColor* backgroundColor;
@property (nonatomic, assign) BOOL belowNavigationBar;
@property (nonatomic, assign) CGFloat widthMenu;
@property (nonatomic, assign) CGFloat percentWidthMenu;

- (void)presentLeftMenuControllerAnimated:(BOOL)animated;
- (void)hideLeftMenuControllerAnimated:(BOOL)animated;
@end
