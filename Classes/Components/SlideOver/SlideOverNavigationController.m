//
//  SlideOverNavigationController.m
//  RTE
//
//  Created by Aymeric De Abreu on 08/10/2015.
//  Copyright © 2015 altays. All rights reserved.
//

#import "SlideOverNavigationController.h"

@interface SlideOverNavigationController () <UIGestureRecognizerDelegate>

@property (nonatomic, strong) NSLayoutConstraint* leadingConstraint;

@property (nonatomic, strong) UIView* backgroundView;
@property (nonatomic, strong) UIScreenEdgePanGestureRecognizer* edgeGesture;

@property (nonatomic, strong) UIPanGestureRecognizer* menuPan;

@end

@implementation SlideOverNavigationController

- (id)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.widthMenu = 262;
    self.percentWidthMenu = -1;
    self.belowNavigationBar = NO;
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    
    UIScreenEdgePanGestureRecognizer* edgePan = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanFromEdge:)];
    edgePan.edges = UIRectEdgeLeft;
    edgePan.delegate = self;
    [self.view addGestureRecognizer:edgePan];
    self.edgeGesture = edgePan;
    
    
    UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleMenuPan:)];
    [self.view addGestureRecognizer:panGesture];
    panGesture.enabled = NO;
    self.menuPan = panGesture;
}


- (void)installMenuViewforAnimation:(BOOL)animated
{
    if (self.leftMenuController && !self.leftMenuController.view.superview) {
        CGRect dstFrame;
        
        self.backgroundView = [[UIView alloc] init];
        UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissMenu)];
        [self.backgroundView addGestureRecognizer:tapGesture];
        self.backgroundView.backgroundColor = self.backgroundColor;
        
        
        if (self.belowNavigationBar) {
            dstFrame = self.topViewController.view.frame;
            self.backgroundView.frame = dstFrame;
        }
        else
        {
            dstFrame = self.view.frame;
            self.backgroundView.frame = dstFrame;
        }
        if (self.percentWidthMenu > 0) {
            dstFrame.size.width *= self.percentWidthMenu;
        }
        else
        {
            dstFrame.size.width = self.widthMenu;
        }
        
        self.leftMenuController.view.frame = dstFrame;
        
        [self.view addSubview:self.backgroundView];
        self.backgroundView.translatesAutoresizingMaskIntoConstraints = NO;
        if (self.belowNavigationBar) {
            [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.backgroundView attribute:(NSLayoutAttributeTop) relatedBy:(NSLayoutRelationEqual) toItem:self.navigationBar attribute:(NSLayoutAttributeBottom) multiplier:1 constant:0]];
            [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.backgroundView attribute:(NSLayoutAttributeBottom) relatedBy:(NSLayoutRelationEqual) toItem:self.view attribute:(NSLayoutAttributeBottom) multiplier:1 constant:0]];
        }
        else
        {
            [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[background]|" options:0 metrics:nil views:@{@"background":self.backgroundView}]];
        }
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[background]|" options:0 metrics:nil views:@{@"background":self.backgroundView}]];
        
        
        
        [self.view addSubview:self.leftMenuController.view];
        [self addChildViewController:self.leftMenuController];
        self.leftMenuController.view.translatesAutoresizingMaskIntoConstraints = NO;
        
        CGFloat cstLeft = 0;
        if (animated) {
            cstLeft = -dstFrame.size.width;
        }
        
        self.leftMenuController.view.translatesAutoresizingMaskIntoConstraints = NO;
        
        self.leadingConstraint = [NSLayoutConstraint constraintWithItem:self.leftMenuController.view attribute:(NSLayoutAttributeLeading) relatedBy:(NSLayoutRelationEqual) toItem:self.view attribute:(NSLayoutAttributeLeading) multiplier:1 constant:cstLeft];
        [self.view addConstraint:self.leadingConstraint];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.leftMenuController.view attribute:(NSLayoutAttributeLeading) relatedBy:(NSLayoutRelationEqual) toItem:self.view attribute:(NSLayoutAttributeLeading) multiplier:1 constant:0]];
        
        if (self.belowNavigationBar) {
            [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.leftMenuController.view attribute:(NSLayoutAttributeTop) relatedBy:(NSLayoutRelationEqual) toItem:self.navigationBar attribute:(NSLayoutAttributeBottom) multiplier:1 constant:0]];
        }
        else
        {
            [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.leftMenuController.view attribute:(NSLayoutAttributeTop) relatedBy:(NSLayoutRelationEqual) toItem:self.view attribute:(NSLayoutAttributeTop) multiplier:1 constant:dstFrame.origin.y]];
        }
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.leftMenuController.view attribute:(NSLayoutAttributeWidth) relatedBy:(NSLayoutRelationEqual) toItem:nil attribute:(NSLayoutAttributeNotAnAttribute) multiplier:1 constant:dstFrame.size.width]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.leftMenuController.view attribute:(NSLayoutAttributeBottom) relatedBy:(NSLayoutRelationEqual) toItem:self.view attribute:(NSLayoutAttributeBottom) multiplier:1 constant:0]];
        
    }
}

- (void)presentLeftMenuControllerAnimated:(BOOL)animated
{
    [self installMenuViewforAnimation:animated];
    
    [self presentLeftMenuControllerAnimated:animated withDuration:0.4 fromAlpha:0];
}


- (void)presentLeftMenuControllerAnimated:(BOOL)animated withDuration:(float)duration fromAlpha:(float)alpha
{
    if (animated) {
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        self.backgroundView.alpha = alpha;
        self.leadingConstraint.constant = 0;
        [UIView animateWithDuration:duration animations:^{
            [self.view layoutIfNeeded];
            self.backgroundView.alpha = 1;
        }];
    }
    self.menuPan.enabled = YES;
}

- (void)hideLeftMenuControllerAnimated:(BOOL)animated
{
    [self hideLeftMenuControllerAnimated:animated withDuration:0.4];
}

- (void)hideLeftMenuControllerAnimated:(BOOL)animated withDuration:(float)duration
{
    if (animated) {
        [self.view layoutIfNeeded];
        self.leadingConstraint.constant = -self.leftMenuController.view.width;
        [UIView animateWithDuration:duration animations:^{
            [self.view layoutIfNeeded];
            self.backgroundView.alpha = 0;
        } completion:^(BOOL finished) {
            [self.leftMenuController.view removeFromSuperview];
            [self.leftMenuController removeFromParentViewController];
            self.leadingConstraint = nil;
            [self.backgroundView removeFromSuperview];
            self.backgroundView = nil;
        }];
    }
    else
    {
        [self.leftMenuController.view removeFromSuperview];
        [self.leftMenuController removeFromParentViewController];
        self.leadingConstraint = nil;
        [self.backgroundView removeFromSuperview];
        self.backgroundView = nil;
    }
    self.menuPan.enabled = NO;
}



- (void)handlePanFromEdge:(UIScreenEdgePanGestureRecognizer*)pan
{
    if (pan.state == UIGestureRecognizerStateBegan) {
        [self installMenuViewforAnimation:YES];
        self.backgroundView.alpha = 0;
    }
    else if (pan.state == UIGestureRecognizerStateChanged)
    {
        self.leadingConstraint.constant = -self.leftMenuController.view.width + [pan translationInView:self.view].x;
        if (self.leadingConstraint.constant > 0) {
            self.leadingConstraint.constant = 0;
        }
        self.backgroundView.alpha = (self.leadingConstraint.constant + self.leftMenuController.view.width)/self.leftMenuController.view.width;
        
    }
    else
    {
        if (fabs(self.leadingConstraint.constant) < self.leftMenuController.view.width/2.0f ) {
            [self presentLeftMenuControllerAnimated:YES withDuration:0.2 fromAlpha:self.backgroundView.alpha];
        }
        else
        {
            [self hideLeftMenuControllerAnimated:YES withDuration:0.2];
        }
    }
}

- (void)handleMenuPan:(UIPanGestureRecognizer*)pan
{
    if (pan.state == UIGestureRecognizerStateBegan) {
    }
    else if (pan.state == UIGestureRecognizerStateChanged)
    {
        self.leadingConstraint.constant = [pan translationInView:self.view].x;
        if (self.leadingConstraint.constant > 0) {
            self.leadingConstraint.constant = 0;
        }
        self.backgroundView.alpha = (self.leadingConstraint.constant + self.leftMenuController.view.width)/self.leftMenuController.view.width;
        
    }
    else
    {
        if (fabs(self.leadingConstraint.constant) < self.leftMenuController.view.width/2.0f ) {
            [self presentLeftMenuControllerAnimated:YES withDuration:0.2 fromAlpha:self.backgroundView.alpha];
        }
        else
        {
            [self hideLeftMenuControllerAnimated:YES withDuration:0.2];
        }
    }
}

- (void)dismissMenu
{
    [self hideLeftMenuControllerAnimated:YES];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer == self.edgeGesture && self.viewControllers.count == 1) {
        return YES;
    }
    else if (gestureRecognizer == self.edgeGesture)
    {
        return NO;
    }
    return YES;
}


@end
