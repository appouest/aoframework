//
//  UIViewController+SlideOver.m
//  RTE
//
//  Created by Aymeric De Abreu on 08/10/2015.
//  Copyright © 2015 altays. All rights reserved.
//

#import "UIViewController+SlideOver.h"
#import "SlideOverNavigationController.h"

@implementation UIViewController (SlideOver)


- (void)presentLeftMenuAnimated:(BOOL)animated
{
    if ([self.navigationController isKindOfClass:[SlideOverNavigationController class]]) {
        [(SlideOverNavigationController*)self.navigationController presentLeftMenuControllerAnimated:animated];
    }
    else if ([self isKindOfClass:[SlideOverNavigationController class]])
    {
        [(SlideOverNavigationController*)self presentLeftMenuControllerAnimated:animated];
    }
    else if ([self.parentViewController isKindOfClass:[SlideOverNavigationController class]])
    {
        [(SlideOverNavigationController*)self.parentViewController presentLeftMenuControllerAnimated:animated];
    }
}

- (void)hideLeftMenuAnimated:(BOOL)animated
{
    if ([self.navigationController isKindOfClass:[SlideOverNavigationController class]]) {
        [(SlideOverNavigationController*)self.navigationController hideLeftMenuControllerAnimated:animated];
    }
    else if ([self isKindOfClass:[SlideOverNavigationController class]])
    {
        [(SlideOverNavigationController*)self hideLeftMenuControllerAnimated:animated];
    }
    else if ([self.parentViewController isKindOfClass:[SlideOverNavigationController class]])
    {
        [(SlideOverNavigationController*)self.parentViewController hideLeftMenuControllerAnimated:animated];
    }
}

- (BOOL)isMenuShown
{
    SlideOverNavigationController* slideOver = nil;
    if ([self.navigationController isKindOfClass:[SlideOverNavigationController class]]) {
        slideOver = (SlideOverNavigationController*)self.navigationController;
    }
    else if ([self isKindOfClass:[SlideOverNavigationController class]])
    {
        slideOver = (SlideOverNavigationController*)self;
    }
    else if ([self.parentViewController isKindOfClass:[SlideOverNavigationController class]])
    {
        slideOver = (SlideOverNavigationController*)self.parentViewController;
    }
    return (slideOver.leftMenuController.view.superview != nil);
}

@end
