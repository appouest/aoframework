//
//  UIViewController+SlideOver.h
//  RTE
//
//  Created by Aymeric De Abreu on 08/10/2015.
//  Copyright © 2015 altays. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (SlideOver)

- (void)presentLeftMenuAnimated:(BOOL)animated;
- (void)hideLeftMenuAnimated:(BOOL)animated;
- (BOOL)isMenuShown;

@end
