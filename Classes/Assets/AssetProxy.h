//
//  AssetProxy.h
//  LPCR
//
//  Created by Aymeric De Abreu on 21/09/2015.
//  Copyright © 2015 appouest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>

typedef NS_ENUM(NSInteger,AssetProxyType)
{
    AssetProxyTypeALAsset = 0,
    AssetProxyTypePHAsset
};

@interface AssetProxy : NSObject

@property (nonatomic, assign) CGSize thumbnailSize;
@property (nonatomic, strong) ALAsset* alAsset;
@property (nonatomic, strong) PHAsset* phAsset;
@property (nonatomic, strong) NSString* identifierHash;
@property (nonatomic, strong) id userInfos;
@property (nonatomic, assign) AssetProxyType assetProxyType;

- (instancetype)initWithALAsset:(ALAsset*)asset;
- (instancetype)initWithPHAsset:(PHAsset*)asset;

- (BOOL)isVideo;
- (BOOL)isPhoto;

- (NSString*)filename;
- (NSString*)mimeType;

- (NSData*)synchronousData;

- (void)asynchronousData:(void(^)(NSData* data))completion;
- (void)thumbnail:(void(^)(UIImage* image))completion;


@end
