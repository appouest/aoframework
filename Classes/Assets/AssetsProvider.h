//
//  AssetLibrary.h
//  Pods
//
//  Created by Jean-Baptiste Denoual on 04/12/2015.
//
//

#import <Foundation/Foundation.h>

@interface AssetsProvider : NSObject

+ (AssetsProvider *)sharedInstance;

- (NSArray *)allPHAssets;
- (void)allALAssets:(void(^)(NSArray* allAssets))completion;

@end
