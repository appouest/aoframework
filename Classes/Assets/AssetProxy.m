//
//  AssetProxy.m
//  LPCR
//
//  Created by Aymeric De Abreu on 21/09/2015.
//  Copyright © 2015 appouest. All rights reserved.
//

#import "AssetProxy.h"

#import <MobileCoreServices/MobileCoreServices.h>

@interface AssetProxy ()

@property (nonatomic, strong) NSDictionary* utiToMimeTypes;

@property (nonatomic, strong) NSString* mType;
@property (nonatomic, strong) NSString* phFilename;

@property (nonatomic, strong) ALAssetsLibrary *library;

@end

@implementation AssetProxy

- (void)commonInit
{
    self.utiToMimeTypes = @{@"public.jpeg":@"image/jpeg",
                            @"public.png":@"image/png",
                            @"public.gif":@"image/gif",
                            @"com.compuserve.gif":@"image/gif"};
}

- (instancetype)initWithALAsset:(ALAsset*)asset
{
    self = [super init];
    self.phAsset = nil;
    self.alAsset = asset;
    self.assetProxyType = AssetProxyTypeALAsset;
    
    NSString *fileName = self.alAsset.defaultRepresentation.filename;
    NSDate *date = [self.alAsset valueForProperty:ALAssetPropertyDate];
    double dateInterval = [date timeIntervalSince1970];
    
    self.identifierHash = [NSString stringWithFormat:@"%@%@",
                           fileName,
                           @(dateInterval)];
    
    [self commonInit];
    return self;
}

- (instancetype)initWithPHAsset:(PHAsset*)asset
{
    self = [super init];
    self.thumbnailSize = CGSizeMake(150, 150);
    self.phAsset = asset;
    self.alAsset = nil;
    self.assetProxyType = AssetProxyTypePHAsset;
    
    self.identifierHash = self.phAsset.localIdentifier;
    
    [self commonInit];
    return self;
}

- (void)thumbnail:(void(^)(UIImage* image))completion
{
    if (self.alAsset) {
        completion([UIImage imageWithCGImage:[self.alAsset thumbnail]]);
    }
    else
    {
        [[PHImageManager defaultManager] requestImageForAsset:self.phAsset targetSize:self.thumbnailSize contentMode:(PHImageContentModeAspectFill) options:nil resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
            completion(result);
        }];
    }
}

- (NSString*)mimeType
{
    if (self.alAsset) {
        ALAssetRepresentation *rep = [self.alAsset defaultRepresentation];
        
        NSString* MIMEType = (__bridge_transfer NSString*)UTTypeCopyPreferredTagWithClass
        ((__bridge CFStringRef)[rep UTI], kUTTagClassMIMEType);
        return MIMEType;
    }
    else
    {
        return self.mType;
    }
}

- (NSData*)synchronousData
{
    if (self.alAsset) {
        ALAssetRepresentation *rep = [self.alAsset defaultRepresentation];
        Byte *buffer = (Byte*)malloc((NSUInteger)rep.size);
        NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:(NSUInteger)rep.size error:nil];
        NSData *fileData = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
        return fileData;
    }
    else
    {

        __block NSData* fileData;
        PHImageRequestOptions* options = [[PHImageRequestOptions alloc] init];
        options.synchronous = YES;
        options.networkAccessAllowed = YES;
        [[PHImageManager defaultManager] requestImageDataForAsset:self.phAsset options:options resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
            fileData = imageData;
            self.mType = self.utiToMimeTypes[dataUTI];
            if ([info objectForKey:@"PHImageFileURLKey"]) {
                // path looks like this -
                NSURL *path = [info objectForKey:@"PHImageFileURLKey"];
                self.phFilename = [path lastPathComponent];
            }
        }];

        return fileData;
    }
}

- (void)asynchronousData:(void(^)(NSData* data))completion
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        if (self.alAsset) {
            
            ALAssetRepresentation *rep = [self.alAsset defaultRepresentation];
            Byte *buffer = (Byte*)malloc((NSUInteger)rep.size);
            NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:(NSUInteger)rep.size error:nil];
            NSData *fileData = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(fileData);
            });
        }
        else
        {
            __block NSData* fileData;
            
            PHImageRequestOptions* options = [[PHImageRequestOptions alloc] init];
            options.synchronous = NO;
            options.networkAccessAllowed = YES;
            
            [[PHImageManager defaultManager] requestImageDataForAsset:self.phAsset options:options resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
                
                fileData = imageData;
                self.mType = self.utiToMimeTypes[dataUTI];
                
                if ([info objectForKey:@"PHImageFileURLKey"]) {
                    
                    // path looks like this -
                    NSURL *path = [info objectForKey:@"PHImageFileURLKey"];
                    self.phFilename = [path lastPathComponent];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(fileData);
                });
            }];

        }
    });
}

- (BOOL)isVideo
{
    if (self.alAsset) {
        return [[self.alAsset valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypeVideo];
    }
    else
    {
        return (self.phAsset.mediaType == PHAssetMediaTypeVideo);
    }
}

- (BOOL)isPhoto
{
    if (self.alAsset) {
        return [[self.alAsset valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto];
    }
    else
    {
        return (self.phAsset.mediaType == PHAssetMediaTypeImage);
    }
}

- (NSString*)filename
{
    if (self.alAsset) {
        return [[[self.alAsset defaultRepresentation] url] lastPathComponent];
    }
    else
    {
        return self.phFilename;
    }
}

@end
