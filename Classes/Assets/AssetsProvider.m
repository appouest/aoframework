//
//  AssetLibrary.m
//  Pods
//
//  Created by Jean-Baptiste Denoual on 04/12/2015.
//
//

#import "AssetsProvider.h"

#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>

@interface AssetsProvider ()

@property (nonatomic, strong) ALAssetsLibrary *library;

@end

@implementation AssetsProvider

+ (AssetsProvider *)sharedInstance
{
    static AssetsProvider *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[AssetsProvider alloc] init];
    });
    
    return sharedInstance;
}


- (void)allALAssets:(void(^)(NSArray* allAssets))completion
{
    __block NSMutableArray *assets = [@[] mutableCopy];
    
    if (NSClassFromString(@"ALAsset")) {
        
        if (!self.library) {
            self.library = [[ALAssetsLibrary alloc] init];
        }
        
        [self.library enumerateGroupsWithTypes:ALAssetsGroupAll
                                    usingBlock: ^(ALAssetsGroup *group, BOOL *stop){
                                        
                                        if (group) {
                                            
                                            [group enumerateAssetsUsingBlock:  ^(ALAsset *result, NSUInteger index, BOOL *stop){
                                                
                                                if (result) {
                                                    
                                                    if ([result valueForProperty:ALAssetPropertyType] == ALAssetTypePhoto) {
                                                        
                                                        [assets addObject:result];
                                                    }
                                                }
                                            }];
                                        }
                                        else {
                                            
                                            // Enumuration completed
                                            assets = [[assets sortedArrayUsingComparator:^NSComparisonResult(ALAsset * _Nonnull obj1, ALAsset * _Nonnull obj2) {
                                                
                                                NSDate *date = [obj1 valueForProperty:ALAssetPropertyDate];
                                                NSDate *date2 = [obj2 valueForProperty:ALAssetPropertyDate];
                                                
                                                return [date2 compare:date];
                                            }] mutableCopy];
                                            
                                            completion(assets);
                                        }
                                    }
         
                                  failureBlock: ^(NSError *error) {
                                      
                                      completion(assets);
                                  }];
    }
    else {
        
        completion(assets);
    }
}

- (NSArray *)allPHAssets
{
    NSMutableArray *assets = [@[] mutableCopy];
    
    if (NSClassFromString(@"PHAsset")) {
        
        PHFetchOptions *allPhotosOptions = [PHFetchOptions new];
        allPhotosOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
        
        PHFetchResult *allPhotosResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:allPhotosOptions];
        
        for (PHAsset *asset in allPhotosResult) {
            [assets addObject:asset];
        }
    }
    
    return assets;
}

@end
