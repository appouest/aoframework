#AOFramework Specs


##Prerequisite

###Add repo
`pod repo add appouest-specs ssh://git@sources.appouest.com:10022/appouest-iOS/Specs.git`

####Test if ok : 
`cd ~/.cocoapods/repos/appouest-specs/`
`pod repo lint .`

##Test your specs :
Inside directory where AOFramework.podspec is.
 
`pod spec lint AOFramework.podspec --verbose --allow-warnings`

##Push your specs : 
Inside directory where AOFramework.podspec is.

`pod repo push appouest-specs AOFramework.podspec --allow-warnings`

