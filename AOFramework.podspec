Pod::Spec.new do |s|
  s.name             = "AOFramework"
  s.version          = "0.1.27"
  s.summary          = "AppOuest Framework"
  s.homepage         = "http://sources.appouest.com/appouest-iOS/AOFramework"
  s.license = { 
                :type => 'Apache 2',
                :file => 'LICENSE' 
              }

  s.authors = { 'Aymeric De Abreu' => 'ada@appouest.com',
                'Jean-Baptiste Denoual' => 'jbd@appouest.com',
                'Flavien Simon' => 'flavien@appouest.com'
               }
  s.source           = { 
                          :git => "http://git@sources.appouest.com:10080/appouest-iOS/AOFramework.git", :tag => s.version.to_s
                        }
  s.social_media_url = 'https://twitter.com/appouest'

  s.ios.deployment_target = '7.0'
  s.tvos.deployment_target = '9.0'
  s.requires_arc = true

  s.source_files = 'Classes/*.{h,m}'

  s.frameworks = 'UIKit'
  s.module_name = 'AOFramework'

  s.subspec 'Foundation' do |sub|
    sub.source_files = 'Classes/Foundation/*.{h,m}'
  end

  s.subspec 'UIKit' do |sub|
    sub.dependency 'SVProgressHUD'
    sub.source_files = 'Classes/UIKit/*.{h,m}'

    sub.subspec 'AOAlertView' do |alertview|
      alertview.dependency 'DQAlertView'
      alertview.source_files = 'Classes/UIKit/AOAlertView/*.{h,m}'
      alertview.platform = :ios
    end

    sub.subspec 'Social' do |social|
      social.source_files = 'Classes/UIKit/Social/*.{h,m}'
      social.platform = :ios
    end
  end

  s.subspec 'Assets' do |sub|
    sub.platform = :ios
    sub.source_files = 'Classes/Assets/*.{h,m}'
    sub.ios.framework = 'MobileCoreServices' , 'AssetsLibrary' , 'Photos'
  end

  s.subspec 'Components' do |sub|
    sub.subspec 'SlideOver' do |slideover|
      slideover.platform = :ios
      slideover.dependency 'AOFramework/UIKit'
      slideover.source_files = 'Classes/Components/SlideOver/*.{h,m}'
    end
    sub.subspec 'AOInternetBanner' do |internetBanner|
      internetBanner.dependency 'CVKHierarchySearcher'
      internetBanner.dependency 'SCNetworkReachability/Multithreading'
      internetBanner.source_files = 'Classes/Components/AOInternetBanner/*.{h,m}'
      internetBanner.platform = :ios
    end
  end

end